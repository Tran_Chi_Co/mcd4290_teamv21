/* Write your function for implementing the Bi-directional Search algorithm here */
function bidirectionalSearchTest(array) 
    {
        let counter = 0;
        let mid = parseInt(array.length/2);
        while(counter <= mid)
        {
            let obj = array[mid-counter];
            if(obj.emergency == true)
                {
                    return obj.address;
                }
            else
                {
                    obj = array[mid+counter];
                    if(obj.emergency == true)
                        {
                            return obj.address;
                        }
                    else
                        {
                            counter++;
                        }
                }
        }
        return null;
    }
