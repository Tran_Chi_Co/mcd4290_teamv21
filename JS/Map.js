"use strict"
// This JS use to display the map and the interactivity with user to help them choose destinations


let tripdata=getDataLocalStorage()
Trip.fromData(tripdata)
let chosenTrip=Trip.gettrip(Trip.counttrip-1)
console.log(chosenTrip)
let destination=[]

let map // global variable for the map




// retrieve Data from API to local storage
let url="https://eng1003.monash/OpenFlights/airports/"
let data=
    {
        "country":Trip.gettrip(Trip.counttrip-1).country,
        "callback":"airport"
    }






webServiceRequest(url,data)
function airport(result)
    {   
        let resultString=JSON.stringify(result)
        localStorage.setItem("airport",resultString)
        let airportInfo=JSON.parse(localStorage.getItem("airport"))
        console.log(airportInfo)
        
        let allowAirport=[]
        for(let i=0;i<airportInfo.length;i++)
            {
                allowAirport.push(parseInt(airportInfo[i].airportId))
            }

        console.log(allowAirport)
        mapboxgl.accessToken = 'pk.eyJ1IjoidHJhbmNoaWNvIiwiYSI6ImNrbnR1NzBicDA1Z2gycG1tM2J1ZHpiNG4ifQ.T4omqQGcye2nro8bcj0ogA';
        let center = [airportInfo[1].longitude, airportInfo[1].latitude];

         map = new mapboxgl.Map
            ({
                container: 'map',
                style: 'mapbox://styles/mapbox/streets-v10',
                zoom: 5,
                center: center
                });


            //Show the marker of all airport
        for(let i=0;i<airportInfo.length;i++)
            {
                let marker = new mapboxgl.Marker().setLngLat([airportInfo[i].longitude, airportInfo[i].latitude]).setPopup(new mapboxgl.Popup().setHTML(airportInfo[i].name)).addTo(map);
                marker.getElement().addEventListener('click', function(){Marker(airportInfo[i])})
            }


    }




// Retrieve info from allroutes API and store to local storage
url="https://eng1003.monash/OpenFlights/allroutes/"
data=
    {
        "country":Trip.gettrip(Trip.counttrip-1).country,
        "callback":"allroute"
    }
webServiceRequest(url,data)
function allroute(result)
    {   
        let resultString=JSON.stringify(result)
        localStorage.setItem("allRoute",resultString)
    }


// THis function will be run whenever user click on the popUp
let airportIdArray=[]
function Marker(info)
    {
        //let a=JSON.parse(localStorage.getItem("allRoute"))
        console.log(info)
        if(chosenTrip.destination.length==0)
            {
                let chosenTripinfo=
                    {
                        airportId: info.airportId,
                        city: info.city,
                        coor:[info.longitude,info.latitude]
                    }
                chosenTrip.adddestination(chosenTripinfo)
                airportIdArray.push(info.airportId)
                console.log(chosenTrip)
                drawSuggestedAirport()
            }
        else
            {
                if (destinationCheck(info.airportId)==true&&airportIdArray.includes(info.airportId)==false) //check if that airport allowed to choose
                    {//if yes, add it infomation to destination
                        let chosenTripinfo=
                            {
                                airportId: info.airportId,
                                city: info.city,
                                coor:[info.longitude,info.latitude]
                            }
                        chosenTrip.adddestination(chosenTripinfo)
                        let coor=[]
                        for(let i=0;i<chosenTrip.destination.length;i++)
                            {
                                coor.push(chosenTrip.destination[i].coor)
                            }
                        console.log(coor)
                        drawSuggestedAirport()
                        removeLayerWithId('chosen')
                        showPath('chosen','#595',coor)
                        airportIdArray.push(info.airportId)

                    }
                else
                    {
                        alert("Please choose an appropriate destination");
                    }
            }
    }







// This functrion  use to check if the chosen airport is allowed
function destinationCheck(id)
    {
        let arriveAirpoetID=[]
        let allRouteinfo=JSON.parse(localStorage.getItem("allRoute"))
        //console.log(chosenTrip.destination[chosenTrip.destination.length-1].airportId)
        console.log(chosenTrip.destination.length)//[chosenTrip.destination.length-1].airportId)
        console.log(allRouteinfo.length)
        for(let i=0;i<allRouteinfo.length;i++)
            {
                //console.log(allRouteinfo[i].sourceAirportId)
                if (allRouteinfo[i].sourceAirportId==chosenTrip.destination[chosenTrip.destination.length-1].airportId) //check if the id of last destination airport = data from allRoute API
                    {
                        arriveAirpoetID.push(allRouteinfo[i].destinationAirportId)
                    }
            }
                //console.log(arriveAirpoetID)
        console.log(typeof(arriveAirpoetID[1]))
        console.log(typeof(parseInt(id)))
                if(arriveAirpoetID.includes(parseInt(id))==true)
                    {return true}
                else 
                    {return false}
    }
    
        
    




let layerArray=[] //use to store all the layer drawn so that can be removed later



// This function is used to provide all the id of choosable destination airport base on the latest chosen destination

function drawSuggestedAirport()
    {        
        let airportInfo=JSON.parse(localStorage.getItem("airport"))
        /*let allowAirport=[]
        for(let i=0;i<airportInfo.length;i++)
            {
                allowAirport.push(parseInt(airportInfo[i].airportId))
            }
        
        console.log(allowAirport)
        */
        let arriveAirpoetID=[]
        let allRouteinfo=JSON.parse(localStorage.getItem("allRoute"))
        console.log(allRouteinfo)
        //console.log(chosenTrip.destination[chosenTrip.destination.length-1].airportId)
        for(let i=0;i<allRouteinfo.length;i++)
            {
                //console.log(allRouteinfo[i].sourceAirportId)
                if (allRouteinfo[i].sourceAirportId==chosenTrip.destination[chosenTrip.destination.length-1].airportId) 
                    //check if the id of last destination airport = data from allRoute API
                    {
                        arriveAirpoetID.push(allRouteinfo[i].destinationAirportId)
                    }
            }
        if(layerArray.length==0) //check if there is some layer in array, if yes delete them before drawing new one
            {
                
            }
        else
           { 
               for(let i=0;i<layerArray.length;i++)
                {
                    removeLayerWithId(layerArray[i])
                }
           }
        for(let i=0;i<airportInfo.length;i++)
            {
                if (arriveAirpoetID.includes(parseInt(airportInfo[i].airportId))==true)
                    {
                        let coor=[]
                        coor.push(chosenTrip.destination[chosenTrip.destination.length-1].coor)
                        coor.push([airportInfo[i].longitude,airportInfo[i].latitude])
                        let id="suggested"+i
                        layerArray.push(id)
                        showPath(id,'#123',coor)
                        
                    }
            }
        console.log(arriveAirpoetID)
        console.log(Array)
        
    }









//THis function help to show on the path to the map
function showPath(sources,color,coor)
    {
            // Code added here will run when the "Show Path" button is clicked.
        map.addSource(sources, {
        'type': 'geojson',
        'data': {
        'type': 'Feature',
        'properties': {},
        'geometry': {
            'type': 'LineString',
            'coordinates': coor    
        }
        }
        });
        map.addLayer({
        'id':sources,
        'type': 'line',
        'source': sources,
        'layout': {
            'line-join': 'round',
            'line-cap': 'round'
        },
        'layer':{ 'type':"LineString"},
        'paint': {
            'line-color': color,
            'line-width': 2
                }
        });
            
    }












function removeLayerWithId(idToRemove)
    {
        let hasPoly = map.getLayer(idToRemove)
        if (hasPoly !== undefined)
            {
                map.removeLayer(idToRemove)
                map.removeSource(idToRemove)
            }
    }





// This function is used to calculate the total distance
function totalDistance()
    {
        let coor=[]
        for(let i=0;i<chosenTrip.destination.length;i++)
            {
                coor.push(chosenTrip.destination[i].coor)
            }
     
        let tripsummary = turf.lineString(coor)
        let distance = turf.length(tripsummary,{units:'kilometers'})
        console.log(coor)
        console.log(distance)
        chosenTrip.distance=distance.toFixed(0)
    }


function Confirm()
    {
       totalDistance()
       console.log(chosenTrip)
       updateLocalStorage(Trip)
        window.location="page4.html"
    }



function undo()
    {   
        if(chosenTrip.destination.length==1)
            {
                chosenTrip.destination.splice(chosenTrip.destination.length-1)
                removeLayerWithId('chosen')
                //delete all the suggested layer
                for(let i=0;i<layerArray.length;i++)
                {
                    removeLayerWithId(layerArray[i])
                }
            }
        else if(chosenTrip.destination.length==0)
            {
                
            }
        else
            {
                chosenTrip.destination.splice(chosenTrip.destination.length-1)
                drawSuggestedAirport()
                let coor=[]
                for(let i=0;i<chosenTrip.destination.length;i++)
                    {
                        coor.push(chosenTrip.destination[i].coor)
                    }
                removeLayerWithId('chosen')
                showPath('chosen','#595',coor)  
            }
    }



function back()
    {
        if (confirm("Are you sure to go back to previous page ?, all of the contents will be lost"))
        {
         // runs if user clicks 'OK'
            Trip.deletetrip(chosenTrip.id)
            //Update Local Storage
            updateLocalStorage(Trip)         
            window.location="page1.html"
        }
        else
        {
         // runs if user clicks 'Cancel'
        }
    }















