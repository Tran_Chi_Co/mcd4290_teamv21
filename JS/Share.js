



const TRIP_INDEX_KEY = "selectedTripIndex";
const TRIP_DATA_KEY = "TripLocalData";



class trip
{
    constructor()
    {
        this._id=0;
        this._name="";
        this._country="";
        this._destination=[];
        this._distance=0;
        this._date="";
        this._time="";
        this._stop=0;
            
    }
    get id()
    {
        return this._id
    }
    get name()
    {
        return this._name
    }
    get country()
    {
        return this._country
    }
    adddestination(data)
    {
        this._destination.push(data)
    }
    get destination()
    {
        return this._destination
    }
    get distance()
    {
        return this._distance
    }
    get date()
    {
        return this._date
    }
    get time()
    {
        return this._time
    }
    get stops()
    {
        return this._stop
    }
    set name(newName)
    {
        this._name=newName
    }
    set id(newId)
    {
        this._id=newId
    }
    set date(newDate)
    {
        this._date=newDate
    }
    set time(newtime)
    {
        this._time=newtime
    }
    set country(newCountry)
    {
        this._country=newCountry
    }
    set distance(newDistance)
    {
        this._distance=newDistance
    }
    fromData(data)
    {
        this._name=data._name;
        this._country=data._country;
        this._destination=data._destination;
        this._distance=data._distance;
        this._date=data._date;
        this._stop=data._stop;
        this._id=data._id
    }
}




class triplist
{
    constructor()
    {
        this._triplist=[];
    }
    addtrip(trip)
    {
        this._triplist.push(trip) 
    }
    gettrip(index)
    {
        return this._triplist[index]
    }
    
    get counttrip()
    {
        return this._triplist.length
    }
    deletetrip(id)
    { for(let i=0;i<this._triplist.length;i++)
        {
            if (this._triplist[i].id== id)
            {
                this._triplist.splice(i,1)
            }
        }

    }
    fromData(data)
    {
        let TripData=data._triplist;
        this._triplist=[]
        for(let i = 0; i < TripData.length; i++)
            {
            let tripinfo = new trip();
            tripinfo.fromData(TripData[i])
            this._triplist.push(tripinfo);
            }
    }
}



// TODO: Write the function checkIfDataExistsLocalStorage
// this function is used to check if any data exist in the TRIP_DATA_KEY


function checkIfDataExistsLocalStorage ()
    {
        let data= getDataLocalStorage();
        if (data)
            {
             return true
            }
        else
            {
             return false
            }
    }






// TODO: Write the function updateLocalStorage
// This function is used to store and update the Triplist and the data of each Trip into local storage
function updateLocalStorage(data)
    { let dataString=JSON.stringify(data)
      localStorage.setItem(TRIP_DATA_KEY,dataString)
        
    }



// TODO: Write the function getDataLocalStorage
// Used to extract the data of Triplist from localstorage
function getDataLocalStorage()
    {
        let dataString=localStorage.getItem(TRIP_DATA_KEY);
        return JSON.parse(dataString)
    }

// TODO: Write the function cancle
function cancle()
    {   
        window.location="Main.html"
    }



// Global triplist instance variable
let Trip = new triplist();






